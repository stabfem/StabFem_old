%%  STOKES FLOW - TUTORIAL 1 FOR MSc 1st year in Paul Sabatier University
%
%  This script has been designed to study the flow around a cylinder 
%  at very low Reynolds numberKB . 
%  We study how this flow is characterised by analysing main properties
%  of flow quantities such as flow field, pressure, vorticity and
%  shear stress.
%
%  
%  The script performs the following calculations :
% 
%  # Generation of a mesh
%  # Computation of the stationary solution at Re = 0.25
%  # Display flow fields
%  # Display velocity profiles along y and x
%  # Display pressure
%  # Display drag coefficient and comparison with theory
%  # Display shear stress
%

%%
%
% First we set a few global variables needed by StabFem
%

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity', 4);

%% Parameters of the simulation
% The mesh is generated with a domain size of [xmin, xmax, ymax] 
% xmin: Distance from the cylinder center to the inlet
% xmax: Distance from the cylinder center to the outlet
% ymax: Distance from the axis of symmetry to the upper boundary.
% Please consider that only half of the computational domain is simulated.
% The flow past a circular infinite cylinder is symmetric with respect
% to the x-axis up to more or less Re=47

xmin = -50; xmax = 50; ymax = 100;

% In this first TP we will deal with the Stokes flow. This means that
% we will consider Re << 1. However, very low Reynolds require large
% computational domains, which means that we need more time to run
% computations. Then we need to find a compromise, the compromise is to set
% a small Re but not too small.

Re = 0.25;

ReText = ['$Re=',num2str(Re),'$'];
%% build mesh and compute steady-state solution
% We compute an initial mesh with the command SF_Mesh
% Then we compute a steady-state with the command SF_BaseFlow and then we
% perform mesh adaptation. The mesh adaptation algorithm refines the mesh
% wherever the steady state, which is passed as an input to SF_Adapt, have
% high gradients. 
ffmesh=SF_Mesh('Mesh_Cylinder_FullDomain.edp','Params',[xmin xmax ymax],'problemtype','2D','symmetry','N');
bf=SF_BaseFlow(ffmesh,'Re',0.25,'type','NEW');
bf = SF_Adapt(bf,'Hmax',10,'InterpError',5e-3);
bf = SF_Adapt(bf,'Hmax',10,'InterpError',5e-3);
bf=SF_BaseFlow(bf,'Re',Re,'type','NEW');


%% Plot the computed mesh
figure();SF_Plot(bf,'mesh','title','mesh (full)');
figure();SF_Plot(bf,'mesh','title','mesh (zoom)','xlim',[-3 3],'ylim',[-3 3]);
pauseKey;
%% plot Ux, Uy, omega, P

figure(1);
subplot(2,2,1); SF_Plot(bf,'ux','Contour','on','xlim',[-5 5],'ylim',[-5 5],'title','u_x');
subplot(2,2,2); SF_Plot(bf,'uy','Contour','on','xlim',[-5 5],'ylim',[-5 5],'title','u_y');
subplot(2,2,3); SF_Plot(bf,'p','Contour','on','xlim',[-5 5],'ylim',[-5 5],'title','p');
subplot(2,2,4); SF_Plot(bf,'vort','Contour','on','xlim',[-5 5],'ylim',[-5 5],'title','\omega');
pauseKey;
%% Streamlines 

figure();
SF_Plot(bf,'psi','Contour','on','xlim',[-20 20],'ylim',[-20 20],'title','\psi','colorrange','cropminmax');
pauseKey;

%% Velocity profiles along y (Vitesse tout au long y)
% Dans cette section nous allons visualiser les champs de vitesse U et V


XMax = xmax; XMin = 0.5; YMax = ymax; YMin = 0.5;
X = [XMin:.01:XMax]; Y = [YMin:.01:YMax];
Vy=SF_ExtractData(bf,'uy',0,Y);
Uy=SF_ExtractData(bf,'ux',0,Y);
Vx=SF_ExtractData(bf,'uy',X,0);
Ux=SF_ExtractData(bf,'ux',X,0);

figure;
subplot(2,2,1); 
plot(Ux,X,'ko-');
title('Velocity $U(x,0)$', 'Interpreter','latex'); legend(ReText,'Interpreter','latex');
subplot(2,2,2); 
plot(Vx,X,'ko-');
title('Velocity $V(x,0)$', 'Interpreter','latex'); legend(ReText,'Interpreter','latex');
xlim([-1,1]);
subplot(2,2,3); 
plot(Uy,Y,'ko-');
title('Velocity $U(0,y)$', 'Interpreter','latex'); legend(ReText,'Interpreter','latex');
subplot(2,2,4); 
plot(Vy,Y,'ko-');
title('Velocity $V(0,y)$', 'Interpreter','latex'); legend(ReText,'Interpreter','latex');




%% Vorticity field
figure;
SF_Plot(bf,'vort','Contour','on','xlim',[-10 10],'ylim',[-10 10],...
        'title',ReText,'colorrange',[-1,1]);
pauseKey;

%% Pressure along the surface
theta = linspace(0,2*pi,200);
Ycircle = 0.501*sin(theta); Xcircle = 0.501*cos(theta); 
% This command is used to extract data (in this case pressure) along a line
pWall = SF_ExtractData(bf,'p',Xcircle,Ycircle); 

figure;
subplot(1,1,1);plot(theta,pWall,'b^');
xlabel('\theta'); ylabel('p'); title('pressure $p(r=R,\theta)$ along the surface', 'Interpreter','latex');
legend(ReText,'Interpreter','latex');
pauseKey;



%% Vorticity at the wall
%Vorticité tout au long la paroi
theta = linspace(0,2*pi,200);
Ycircle = 0.5*sin(theta); Xcircle = 0.5*cos(theta); 
% VortWall = Calcule moi avec SF_ExtractData à partir des definitions
% de dxu, dyu dxv et dyv! (qui sont les dérives de u par rapport x, ...)

%% Shear along the surface
theta = linspace(0,2*pi,200);
Ycircle = 0.5*sin(theta); Xcircle = 0.5*cos(theta); 
% tauWall = Calcule moi avec SF_ExtractData à partir des definitions
% de tauxx, tauxy et tauyy!

%% Drag coefficient
% Theoretical first order prediction (Van Dyke)
gamma = 0.5772156649015328606065120900824024310421; % Euler-Mascheroni const
delta = 1.0/(0.5-gamma-log(Re/4));
CdTh = 4*pi/Re*(delta - 0.87*delta^3);
CdNum =  bf.Fx;
errorRel = (CdTh-CdNum)/(CdTh);

% Calcul la trainée à partir de SF_ExtractData et comparez ce résultat 
% avec CdTh et CdNum

%% Calculer la traînée avec SF_ExtractData et tauWall




