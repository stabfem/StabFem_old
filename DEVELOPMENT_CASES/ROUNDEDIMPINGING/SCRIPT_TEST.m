%%  Instability of a impinging rounded jet
%
% 
% THIS SCRIPT GENERATES PLOTS FOR THE FORCED STRUCTURES AND THE
% EIGENMODES FOR THE IMPINGING ROUNDED JET
% 
% REFEFENCE : Giannetti & Sierra 
%
%
% strBFFileName='../../../../../../Orr/WORK/BaseFlow.ff2m'
% strMeshFileName='../../../../../../Orr/Data/Mh.msh'
% mesh = SFcore_ImportMesh(strMeshFileName);
% bf = SFcore_ImportData(ffmesh,strBFFileName);

%% Chapter 0 : Initialization 
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%% Create an initial mesh
ffmesh = SF_Mesh('Mesh_nonlin.edp','problemtype','axicompsponge','cleanworkdir','no');
%% Start computation of baseflow
Ma = 0.1;
bf=SF_BaseFlow(ffmesh,'Re',10,'Mach',Ma,'type','NEW');
bf=SF_BaseFlow(bf,'Re',50,'Mach',Ma,'type','NEW');
Ma = 0.3;
bf=SF_BaseFlow(bf,'Re',50,'Mach',Ma,'type','NEW');
for Re = [100:25:2000]
    bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW');
    MaskPipe = SF_Mask(bf.mesh,[-5.5 5.5 0.0 0.75 .05]);
    MaskImpPipe = SF_Mask(bf.mesh,[0.0 5.0 0.0 120 .25]);
    bf=SF_Adapt(bf,MaskPipe,MaskImpPipe,'Hmax',5.0);
end
Ma = 0.4;
bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW');
Ma = 0.5;
bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW');
for Re = [2025:25:3000]
    bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW');
    MaskPipe = SF_Mask(bf.mesh,[-5.5 5.5 0.0 0.75 .05]);
    MaskImpPipe = SF_Mask(bf.mesh,[0.0 5.0 0.0 120 .25]);
    bf=SF_Adapt(bf,MaskPipe,MaskImpPipe,'Hmax',5.0);
end
%% Plot velocity profiles at the outlet of the pipe
Y = [0.0:0.005:0.5];
ux = SF_ExtractData(bf,'ux',-0.1,Y);
M = SF_ExtractData(bf,'MachField',-0.1,Y);
figure; plot(Y,M,'r');
ftan=0.1;
uxFv=1.8*tanh((1./2.-Y)/ftan)/tanh((1./2.)/ftan);
figure; plot(Y,ux,'r',Y,uxFv,'k');

%% Adapt baseflow for higher precision in the identification of modes
MaskPipe = SF_Mask(bf.mesh,[-5.5 5.5 0.0 0.75 .05]);
MaskImpPipe = SF_Mask(bf.mesh,[0.0 5.0 0.0 120 .25]);
MaskImpPipeLow = SF_Mask(bf.mesh,[0.0 5.0 0.0 30 .1]);
bf=SF_Adapt(bf,MaskPipe,MaskImpPipe,MaskImpPipeLow,'Hmax',5.0);

%% Linear stability 
shiftList = [0:0.1:4.0]; Nshift = length(shiftList);
EVList = []; EMList = [];

for j=[1:Nshift]
    shift = 0.2+1i*shiftList(j);
    [ev,em] = SF_Stability(bf,'shift',shift,'nev',10,'type','D','m',0);
    EVList = [EVList,ev]; EMList = [EMList, em];
end


mode1 = EMList(321);
mode2 = EMList(341);
mode3 = EMList(381);
mode4 = EMList(1);
MaskPipe = SF_Mask(bf.mesh,[-5.5 5.5 0.0 0.75 .05]);
MaskImpPipe = SF_Mask(bf.mesh,[0.0 5.0 0.0 120 .25]);
MaskImpPipeLow = SF_Mask(bf.mesh,[0.0 5.0 0.0 30 .1]);
bf=SF_Adapt(bf,MaskPipe,MaskImpPipe,MaskImpPipeLow,mode1,mode2,mode4,'Hmax',5.0);
[ev,em] = SF_Stability(bf,'shift',mode1.lambda,'nev',10,'type','D','m',0);
[ev2,em2] = SF_Stability(bf,'shift',mode2.lambda,'nev',10,'type','D','m',0);
[ev3,em3] = SF_Stability(bf,'shift',mode3.lambda,'nev',10,'type','D','m',0);
[ev4,em4] = SF_Stability(bf,'shift',mode4.lambda,'nev',10,'type','D','m',0);
[ev5,em5] = SF_Stability(bf,'shift',EMList(382).lambda,'nev',10,'type','D','m',0);

shiftList = [4.25:0.25:7.0]; Nshift = length(shiftList);

EVListAdapt = []; EMListAdapt = [];
for j=[1:Nshift]
    shift = 0.4+1i*shiftList(j);
    [ev,em] = SF_Stability(bf,'shift',shift,'nev',10,'type','D','m',0);
    EVListAdapt = [EVListAdapt,ev]; EMListAdapt = [EMListAdapt, em];
end

% Spectrum 
plot(real(EVListAdapt),imag(EVListAdapt),'ko');
hold on;
plot(real(ev),imag(ev),'ro')
plot(real(ev2),imag(ev2),'rx')
plot(real(ev3),imag(ev3),'rs')
plot(real(ev4),imag(ev4),'r^')
plot(real(ev5),imag(ev5),'ro')

%% Adjoint + sensitivity to peaks
% Greater peak
modePeak1 = EMListAdapt(51);
[evA,emA] = SF_Stability(bf,'shift',modePeak1.lambda,'nev',5,'type','A','m',0);
modeAPeak1 = emA(1);
SF_SetMapping(bf.mesh, 'MappingType', 'box', 'MappingParams', [-7.5,40,-2,50,-0.0,5,-0.0,5]) ; % For sensitivity inner region (no sponge);
sensitivityPeak1 = SF_Sensitivity(bf,modePeak1,modeAPeak1,'Type','S');
% Second peak
modePeak2 = EMListAdapt(52);
modeAPeak2 = emA(2);
sensitivityPeak2 = SF_Sensitivity(bf,modePeak2,modeAPeak2,'Type','S');
% Recirculation region
shift = EVList(1);
[ev,em] = SF_Stability(bf,'shift',shift,'nev',5,'type','D','m',0);
modePeak3 = em(1);
[evA,emA] = SF_Stability(bf,'shift',shift,'nev',5,'type','A','m',0);
modeAPeak3 = emA(1);
sensitivityPeak3 = SF_Sensitivity(bf,modePeak3,modeAPeak3,'Type','S');
%% Figures of structural endogeinity
subplot(2,3,1);
SF_Plot(sensitivityPeak1,'structEndo','xlim',[-8.0,5.0],'ylim',[0,5],...
        'ColorMap','dawn','Colorbar','off','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k');
title('$\mathcal{R}e \big (S_1(x, y) \big)$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,2); 
SF_Plot(sensitivityPeak2,'structEndo','xlim',[-8.0,5.0],'ylim',[0,5],...
        'ColorMap','dawn','Colorbar','off','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k');
title('$\mathcal{R}e \big (S_2(x, y) \big)$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,3); 
SF_Plot(sensitivityPeak3,'structEndo','xlim',[-8.0,5.0],'ylim',[0,15],...
        'ColorMap','dawn','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k');
title('$\mathcal{R}e \big (S_3(x, y) \big)$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,4);
SF_Plot(sensitivityPeak1,'structEndo.im','xlim',[-8.0,5.0],'ylim',[0,5],...
        'ColorMap','dawn','Colorbar','off','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k');
title('$\mathcal{I}m \big (S_1(x, y) \big)$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,5); 
SF_Plot(sensitivityPeak2,'structEndo.im','xlim',[-8.0,5.0],'ylim',[0,5],...
        'ColorMap','dawn','Colorbar','off','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k');
title('$\mathcal{I}m \big (S_2(x, y) \big)$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,6); 
SF_Plot(sensitivityPeak3,'structEndo.im','xlim',[-8.0,5.0],'ylim',[0,15],...
        'ColorMap','dawn','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k');
title('$\mathcal{I}m \big (S_3(x, y) \big)$','Interpreter','Latex')
set(gca,'FontSize',24);

%% Figures of adjoint + sensitivity
figure;
subplot(2,3,1); set(gca, 'Position', [0.025, 0.5, 0.275, 0.405]);
SF_Plot(sensitivityPeak1,'structSen','xlim',[-8.0,5.0],'ylim',[0,5],...
        'ColorMap','dawn','Colorbar','off','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k');
title('$S_1(x, y)$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,2); set(gca, 'Position', [0.35, 0.5, 0.275, 0.405]);
SF_Plot(sensitivityPeak2,'structSen','xlim',[-8.0,5.0],'ylim',[0,5],...
        'ColorMap','dawn','Colorbar','off','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k');
title('$S_2(x, y)$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,3); set(gca, 'Position', [0.675, 0.5, 0.275, 0.405]);
SF_Plot(sensitivityPeak3,'structSen','xlim',[-8.0,5.0],'ylim',[0,15],...
        'ColorMap','dawn','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k');
title('$S_3(x, y)$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,4); set(gca, 'Position', [0.025, 0.05, 0.275, 0.405]);
SF_Plot(modeAPeak1,'vort','xlim',[-8.0,5.0],'ylim',[0,5],...
        'ColorMap','dawn','Colorbar','off','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k','ColorRange',[-200,200]);
title('$\mathcal{R}e \omega_1$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,5); set(gca, 'Position', [0.35, 0.05, 0.275, 0.405]);
SF_Plot(modeAPeak2,'vort','xlim',[-8.0,5.0],'ylim',[0,5],...
        'ColorMap','dawn','Colorbar','off','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k','ColorRange',[-200,200]);
title('$\mathcal{R}e \omega_2$','Interpreter','Latex')
set(gca,'FontSize',24);

subplot(2,3,6); set(gca, 'Position', [0.675, 0.05, 0.275, 0.405]);
SF_Plot(modeAPeak3,'vort','xlim',[-8.0,5.0],'ylim',[0,15],...
        'ColorMap','dawn','Boundary','on',...
        'BDLabels',[2,21],'BDColors','k','ColorRange',[-5,5]);
title('$\mathcal{R}e \omega_3$','Interpreter','Latex')
set(gca,'FontSize',24);
%%
SF_Plot(bf,'vort','xlim',[-6 5],'ylim',[0 20],'cbtitle',...
    '\omega_z','colormap','redblue','colorrange',[-2 2]);
hold on; SF_Plot(bf,'psi','contour','only',...
       'clevels',[-0.3:0.01:0.05],'xlim',[-6 5],'ylim',[0 20]);
save('restartROUNDEDIMPINGING.mat')