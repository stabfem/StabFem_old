function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of an airfoil  
%  - Base flow with zero-flow (conduction state)
%  - Linear stability of this base-flow
%  - Consctruction of a saturated 2D state with rolls (bifurcated base flow)
%  - 3D instability of this state.
%
% USAGE : 
% autorun -> automatic check (non-regression test). 
% autorun(4) -> verbose mode ; autorun(6) -> debug mode.
% Result "value" is the number of unsuccessful tests


if(nargin==0) 
    verbosity=0; 
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');
format long;

ffmesh = SF_Mesh('Mesh_TORNADO.edp','Params',[.5 10 5 5 1 6 2],'problemtype','axixrswirl');
bf = SF_BaseFlow(ffmesh,'Re',50,'Omegax',1);

%%
% progressively increase Re
bf = SF_Adapt(bf)

up = SF_ExtractData(bf,'uphi',-.5,.5);
upREF = 0.001250234223018

SFerror(1) = abs(up/upREF-1)

value = sum((SFerror>1e-2))



end
