close all; clear all;
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB/']);
SF_Start('verbosity',2);

%%
% Generate a mesh and a starting flow
ffmesh = SF_Mesh('Mesh_TORNADO.edp','Params',[.5 10 5 5 1 8 2],'problemtype','axixrswirl');
bf = SF_BaseFlow(ffmesh,'Re',10,'Omegax',1);

%%
% progressively increase Re
bf = SF_BaseFlow(bf,'Re',50);
bf = SF_BaseFlow(bf,'Re',100);
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf,'Re',500);
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf,'Re',1000);
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf,'Re',2000);
bf = SF_Adapt(bf,'Hmax',.15);
bf = SF_BaseFlow(bf,'Re',3000);
bf = SF_Adapt(bf,'Hmax',.15);

%%
% plot baseflow
psilevels = [ 0 .05:.05:.2 .23 .24 .245 .25 .255 .26 .27 ]/2;
figure;SF_Plot(bf,'vort','xlim',[-5 2],'ylim',[0 5],'cbtitle','\omega_z','colorrange',[-5 5]);
hold on;SF_Plot(bf,'psi','contour','on','clevels',psilevels,'cstyle','patchdashedneg','xystyle','off');

%%
% compute eigenmodes for m=1 and m=2 
[ev1,em1] = SF_Stability(bf,'shift',.2-.2i,'nev',20,'m',1,'sort','lr','plotspectrum','yes')
[ev2,em2] = SF_Stability(bf,'shift',.2-.2i,'nev',20,'m',2,'sort','lr','plotspectrum','yes')

%%
% plot the eigenmodes
figure;SF_Plot(em1(12),'ur','title','unstable eigenmode with m=1');
figure;SF_Plot(em2(15),'ur','title','unstable eigenmode with m=2');

%% 
% Structural sensitivity
[ev,ed] = SF_Stability(bf,'shift',ev1(1),'nev',1,'type','D','m',1);
[ev,ea] = SF_Stability(bf,'shift',ev1(1),'nev',1,'type','A','m',1);
es = SF_Sensitivity(bf,ed,ea);
figure;SF_Plot(es,'sensitivity','title','sensitivity of leading eigenmode with m=1');

% Structural sensitivity
[ev,ed] = SF_Stability(bf,'shift',ev2(1),'nev',1,'type','D','m',2);
[ev,ea] = SF_Stability(bf,'shift',ev2(1),'nev',1,'type','A','m',2);
es = SF_Sensitivity(bf,ed,ea);
%figure;SF_Plot(es,'sensitivity','title','sensitivity of leading eigenmode with m=2');


%% Test to launch a second script subordinated to this one
%publish('SCRIPT_TORNADO2.m') -> does not work

%[[PUBLISH]]
