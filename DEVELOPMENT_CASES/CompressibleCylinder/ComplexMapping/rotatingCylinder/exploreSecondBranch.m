clear all;
% close all;
run('../../../../SOURCES_MATLAB/SF_Start.m');
figureformat='png'; AspectRatio = 0.56; % for figures
tinit = tic;
verbosity=20;


ffmesh = SFcore_ImportMesh('./WORK/MESHES/mesh_adapt27_Re200_Ma0.3_Omegax3.msh',...
                            'problemtype','2dcomp');

mesh.xinfv = 80;
mesh.xinfm = 40;
mesh.yinf = 40;
Params =[mesh.xinfv*0.5 mesh.xinfm*0.5 10000000 mesh.xinfv*0.6 -0.3 mesh.yinf*0.5 10000000 mesh.yinf*0.6 -0.3]; % x0, x1, LA, LC, gammac, y0, LAy, LCy, gammacy
ffmesh = SF_SetMapping(ffmesh,'MappingType','box','MappingParams',Params);
bf=SF_BaseFlow(ffmesh,'Re',1,'Mach',0.01,'Omegax',3.1,'type','NEW');
bf=SF_BaseFlow(bf,'Re',10,'Mach',0.01,'Omegax',3.1,'type','NEW');
bf=SF_BaseFlow(bf,'Re',60,'Mach',0.01,'Omegax',3.1,'type','NEW');
bf=SF_BaseFlow(bf,'Re',100,'Mach',0.01,'Omegax',3.1,'type','NEW');
bf=SF_BaseFlow(bf,'Re',150,'Mach',0.01,'Omegax',3.1,'type','NEW');
bf=SF_BaseFlow(bf,'Re',200,'Mach',0.01,'Omegax',3.1,'type','NEW');
bfInit = bf;
%%

Re = 200;
alphaList=[3.1:0.5:6];
MaList=[0.01,0.1,0.3];
kList = [5:0.5:8];
bf = bfInit;
evListRe200 = [];
kListRe200 = [];
evG = 0;
kG = 5;
shift = 0.1;

for Ma=MaList
    for alpha=alphaList
        bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'Omegax',alpha);
        for k=kList
            [ev,em] = SF_Stability(bf,'k',k,'shift',shift,'nev',1,'type','D');% to initiate the cont mode 
            if(real(ev) > real(evG))
                evG = ev;
                kG = k;
            end
        end
        evListRe200=[evListRe200,evG];
        kListRe200=[kListRe200,kG];
        kList = [kG*0.75:0.1*kG:kG*1.25];
        shift = evG;
    end
end


Re = 150;
alphaList=[3.1:0.5:6];
MaList=[0.01,0.1,0.3];
kList = [2:0.5:5];
bf = bfInit;
evListRe150 = [];
kListRe150 = [];
evG = 0;
kG = 2;
shift = 0.1;
for Ma=MaList
    for alpha=alphaList
        bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'Omegax',alpha);
        for k=kList
            [ev,em] = SF_Stability(bf,'k',k,'shift',shift,'nev',1,'type','D');% to initiate the cont mode 
            if(real(ev) > real(evG))
                evG = ev;
                kG = k;
            end
        end
        evListRe150=[evListRe150,evG];
        kListRe150=[kListRe150,kG];
        kList = [kG*0.75:0.1*kG:kG*1.25];
        shift = evG;
    end
end

Re = 100;
alphaList=[3.1:0.5:6];
MaList=[0.01,0.1,0.3];
kList = [0.1:0.5:2];
bf = bfInit;
evListRe100 = [];
kListRe100 = [];
evG = 0;
kG = 1;
shift = 0.1;
for Ma=MaList
    for alpha=alphaList
        bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'Omegax',alpha);
        for k=kList
            [ev,em] = SF_Stability(bf,'k',k,'shift',shift,'nev',1,'type','D');% to initiate the cont mode 
            if(real(ev) > real(evG))
                evG = ev;
                kG = k;
            end
        end
        evListRe100=[evListRe100,evG];
        kListRe100=[kListRe100,kG];
        kList = [kG*0.75:0.1*kG:kG*1.25];
        shift = evG;
    end
end




%% TESTS
% bf=SF_BaseFlow(bf,'Re',200,'Mach',0.2,'Omegax',3.0,'type','NEW');
% bf=SF_BaseFlow(bf,'Re',200,'Mach',0.1,'Omegax',3.0,'type','NEW');
% bf=SF_BaseFlow(bf,'Re',200,'Mach',0.01,'Omegax',3.0,'type','NEW');
% bf=SF_BaseFlow(bf,'Re',200,'Mach',0.01,'Omegax',3.1,'type','NEW');
% [ev,em] = SF_Stability(bf,'k',1,'shift',0.1,'nev',5,'type','D');% Stable
% [ev,em] = SF_Stability(bf,'k',6,'shift',0.1,'nev',5,'type','D');% Unstable





