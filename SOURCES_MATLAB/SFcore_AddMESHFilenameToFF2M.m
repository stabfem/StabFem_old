function res = SFcore_AddMESHFilenameToFF2M(file,meshfilename,bffilename)

SF_core_log('d','entering SFcore_AddMESHFilenameToFF2M')

if SF_core_getopt('isoctave')
  SF_core_log('d','debugging SFcore_AddMESHFilenameToFF2M with octave')
  %return
end

if (nargin<3)
    bffilename = '';
end

if ~isempty(strfind(file,'.txt'))||~isempty(strfind(file,'.msh')) % NB contains is better but not available with octave !
    file = [file(1:end-4), '.ff2m'];
end

% Read txt into cell A
%fid = fopen([SF_core_getopt('ffdatadir'),file],'r')

if ~exist(file,'file')
  file = [SF_core_getopt('ffdatadir'),file];
  if ~exist(file,'file')
    SF_core_log('e',['could not open file ',file, ' in SFcore_AddMESHFilenameToFF2M']);
  end
else
    SF_core_log('w','in SFcore_AddMESHFilenameToFF2M : File exists in root directory ! this may lead to incorrect behavior'); 
end

fid = fopen(file,'r');
%if fid<1 
%    fid = fopen([SF_core_getopt('ffdatadir'),file],'r')
%    if fid<1
%      SF_core_log('w',['could not open file ',file, ' in SFcore_AddMESHFilenameToFF2M'])
%      return
%    end
%end
i = 1;
tline = fgetl(fid);
A{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(fid);
    A{i} = tline;
end
fclose(fid);

% Changes third line

if ~isempty(strfind(file,'meshfilename'))
    SF_core_log('w',' in SFcore_AddMESHFilenameToFF2M : file already contains a "meshfilename" field');
end
    
A{3} = [ A{3}, ' meshfilename ',meshfilename];
%t = A{3}
if ~isempty(bffilename)
    A{3} = [ A{3}, ' baseflowfilename ',bffilename];
end

% Writes back the file

fid = fopen(file, 'w');
for i = 1:numel(A)
    if A{i+1} == -1
        fprintf(fid,'%s', A{i});
        break
    else
        fprintf(fid,'%s\n', A{i});
    end
end
fclose(fid);

SF_core_log('d',[' added meshfilename ',meshfilename,' to file ',file]);

res = 0;

end
