% !TEX root = main.tex

\chapter{Advanced drivers for global stability studies}

The purpose of this chapter is to demonstrate the usage of the stability-oriented drivers of the project :
\SF{BaseFlow}, \SF{Stability} and \SF{LinearForced}.

The presentation will be done on a standard problem, namely the flow around a circular cylinder.

This chapter is expected to be studied in parallel to the examples CYLINDER\_Linear, 
CYLINDER\_PostProcess (?) ....

A research paper was also published on this case and we refer to it for a more in-depth exposition of the theoretical bases.
\begin{figure}
\setlistmatlab
\lstinputlisting{SCRIPT_CYLINDER_SHORT.m}
\caption{
Matlab program \mlfile{CYLINDER\_LINEAR.m} (simplified version)}
\label{fig:SCRIPT_CYLINDER.m}
\end{figure}

\begin{figure}
\includegraphics[width= .48\linewidth]{../../STABLE_CASES/CYLINDER/FIGURES/Cylinder_Mesh.png}
\includegraphics[width= .48\linewidth]{../../STABLE_CASES/CYLINDER/FIGURES/Cylinder_BaseFlowRe60.png}
\caption{Mesh and Base flow for the flow over a cylinder at $Re=60$. Pressure field (color levels) and streamlines (iso-levels of the streamfunction $\psi$). }
\label{fig:Baseflow}
\end{figure}

\begin{figure}
\includegraphics[width= .48\linewidth]{../../STABLE_CASES/CYLINDER/FIGURES/Cylinder_EigenModeRe60_AdaptS.png}
\includegraphics[width= .48\linewidth]{../../STABLE_CASES/CYLINDER/FIGURES/SpectrumExplorator.png}
\caption{Leading linear eigenmode for the wake of a cylinder with $Re = 100$ ($a$), 
and spectrum ($b)$. Figure $(b)$ is generated using the "spectrum explorator" mode by specifying the \mlcode{'PlotSpectrum',true} option. Clicking on the eigenvalue with larges real part will result in a generation of figure ($a$).}
\label{fig:Eigenmode}
\end{figure}

\section{ Physical problem  and main script}

We start from the Navier-Stokes equations written symbolically as follows:
%
\begin{eqnarray} \label{NSprimitive}
\partial_t {\bf u} = {\cal NS} ({\bf u},p)
\equiv - {\bf u} \cdot \nabla {\bf u} - \nabla p + \frac{2}{Re}  \nabla \cdot {\mathsf{D}}({\bf u}),  \\
\nabla \cdot {\bf u} = 0,
\end{eqnarray}
%
In a first step we compute a base flow $({\bf u_0},p_0)$ as a steady solution of the nonlinear problem:
$$
 {\cal NS} ({\bf u}_0,p_0)
 = 0
$$

In a second step we compute the eigenmodes as solutions of:

$$
\lambda \hat{{\bf u}} = {\cal LNS}_{{\bf u}_b}( \hat{\bf u},\hat{p}),
$$


Figure \ref{fig:SCRIPT_CYLINDER.m} shows a StabFem script doing the analysis.

Figure \ref{fig:Baseflow} show the mesh and base flow.


Figure \ref{fig:Baseflow} show the eigenmode and the spectrum.







\section{ Analysis of the example and presentation of the drivers }

\subsection{Initializations and initial mesh generation}


The first lines of the script are similar as in the example of the last chapter and should normally be present at the top of any StabFem script. Let us explain these lines now:

\begin{itemize}

\item  First line is to give Matlab/Octave access to the directory containing all drivers. It is equivalent to \mlcode{addpath('../../SOURCES\_MATLAB')} but uses a syntax which prescribes an absolute path.

\item Second line, invocation of \SF{core\_start} performs a series of initialization (check that \freefem is correctly installed and which solvers are available, detect the paths ...) 

\item Third line is to set one important global option controlling the operation of StabFem.  \mlcode{'verbosity'} controls the amount of messages returned by the interface. The value is expected between 0 and 8, larger values lead to more messages.
The recommended values are '3' (only important messages from the drivers) or '4' (important messages from the drivers  plus standard output of FreeFem solvers). '6' or more is for debugging and '0' is reserved for internal tests. 

\item Fourth line is to initialize the Database manager by precising the working directory.
Here, the files produced by the FreeFem solvers will be written in the directory \shell{./WORK/} and will be subsequently stored by the drivers in dedicated subdirectories of this folder.   
See section ?? of this chapter about postprocessing to see how to access to rthe content of this database.
\end{itemize}

\Remark{ Steps 2,3,4 of this initialization can be regrouped in a single line by calling  of \SF{Start}.  In this way, default values will be used (3 for verbosity and \shell{./WORK/} for ffdatadir).}


\subsubsection*{Initial mesh generation :}

An initial mesh is generated using \SF{Mesh} using a valid \shell{.edp} file generating a mesh. This step has already been described in the previous chapter and will be discussed in much longer detail in a next chapter.

It is important here to notice the use of the parameter \mlcode{'problemtype'}  This setting defines the general class of problems to which the problem belongs and is essential to tell to the drivers which solvers have to be used. 

Here the value  \mlcode{'2D'} means that the problem is a 2-dimensional and incompressible flow, and that the FreeFem solvers \fffile{Newton\_2D.edp} and  \fffile{Stab2D.edp} will be used.


\subsection{ Computing a steady flow : \SF{BaseFlow} }

The computation of the base flow using Newton method is monitored using the generic driver function \SF{BaseFlow} which is invoked three times in the script.

Analysis of the example shows that   \SF{BaseFlow}  had the following syntax:

\begin{itemize}
\item 
 The first argument is either a 'mesh' object previously generated using \SF{Mesh} or a flowfield object generated by a previous call to \SF{BaseFlow} (or from any other driver generating a flowfield object ; for instance the result of a DNS could also be used here).

If the first argument to the function is a flowfield, it it  will be used as a 'guess' to start the Newton iteration. Otherwise an arbitrary initial condition will be used to initialize the iteration.



\item The next is a series of optional parameters coming as descriptor/value pairs.
Here, considering the '2D' class of problems, only one physical parameter, the Reynolds number identified as \mlcode{'Re'}, is relevant. For other classes of problems,  additional 
parameters, such as the Mach number for compressible problems, etc...) could be passed in the same way.




\end{itemize}


\Tips{
\item 
Note that in the present example we initially compute a first base flow in a highly viscous case , namely $Re = 1$, and subsequently raise the $Re$ to 60. It is advised to proceed in this way by gradually varying the parameters (continuation method), since the Newton method works best if the 'guess' is not too far from the actual solution.

\item In some classes of problems where multiple solutions can be expected using a given set of parameters, it is possible to use pseudo-arclength continuation. See example ?? on the website to see how to do this.

\item Normally the \freefem solver is automatically detected according to the keyword \mlcode{'problemtype'}  but you may specify a different solver using option \mlcode{'solver'}. For instance  \mlcode{SF\_BaseFlow(bf,'Re,10,'solver','MyNewton.edp' )} will use \fffile{MyNewton.edp} instead of \fffile{Newton\_2D.edp}. When doing this your custom solver must have the same input/outputs as the default one.
This method may be used in development stages to test an alternative solver ; however it is much more advised to stick to the default solvers and use custmizable macros for adaptation to your needs (see next section ???).

\item A number of other options are also recognized. 
For a full list of available options see the documentation by typing \mlcode{help SF\_BaseFlow
}.  
}



%\begin{figure*}[t]
%\setlistfreefem
%\lstinputlisting{../../SOURCES_FREEFEM/Mesh_Template.edp}
%\caption{
%\freefem program \fffile{Mesh\_Template.edp})}
%\label{Mesh_Template.edp}
%\end{figure*}


\subsection{Performing an eigenvalue analysis : \SF{Stability} }

The second stage of the global stability analysis, nemally {\em eigenvalue computation}, is handled using the driver \SF{Stability}.

Here again, the first argument of this function is expected to be a flowfield object (here called \mlcode{bf} ). A {\em base flow} previously computed using \SF{BaseFlow} is generally expected (although a meanflow, or a DNS snapshot, could also be used).

Note that in some classes of problems in which the notion of baseflow is not relevant (for instance linear acoustics, of sloshing of static free-surface equilibria) a mesh object can also be provided as the first argument of  \SF{Stability} (explore the various examples on the website to see this, for instance \shell{ACOUSTIC\_PIPES}).

The next argument are again options provided as pairs of descriptor/values. These options comprise physical parameters, numerical parameters and post-processing parameters.

As for {\em physical parameters:}

\begin{itemize}
\item  \mlcode{'symmetry'} is used for problems where the base flow admits a symmetry plane, to specify if one wants antisymmetric modes (with value \mlcode{'A'}) or symmetric ones (with value \mlcode{'S'}).

Other physical parameters can be specified, depending opn the classes of problems. For instance, for 2D-incompressible problems an axial wavenumber \mlcode{'k'} can be specified to study 3D instabilities of a 2D flow. For axisymmetric problems an azimuthal wavenumber 
\mlcode{'m'} can be similarly specified.


\end{itemize}

As for {\em numerical parameters:}


\begin{itemize}
\item \mlcode{'nev'} specifies the number of eigenvalues requested. For $nev = 1$ the simple shift-invert algorithm is used while for $nev>1$ Arnoldi method is used using  Arpack or SLEPc librairies.

\item \mlcode{'shift'} is the "shift" value of the shift-invert and Arnoldi methods. A complex value is expected, but you may also use the keywords \mlcode{'prev'}  to use the last computed eigenvalue or  \mlcode{'cont'} to use an interpolation fro the two last eigenvalues (continuation mode).

\item \mlcode{'type'} is either 'D' (direct modes, default value) or 'A' (adjoint modes).

\end{itemize}

Among {\em post-processing parameters:}


\begin{itemize}
\item \mlcode{'sort'} allows to sort the eigenvalues. Use \mlcode{'LR'} to sort according to largest real part, \mlcode{'LI'} to sort according to largest imaginary part, etc...

\item The option \mlcode{'PlotSpectrum',true} will launch the 'spectrum explorator' mode. The computed spectra will be plotted in a figure (see figure  and it is possible to plot any of the computed eigenmodes simply by clicking on the corresponding eigenvalue.

Note that in spectrum explorator mode, spectra computed by successive calls to \SF{Stability} (for instance by varying the shift to explore several parts of the complex plane) will be superposed in figure 100. To reinitialize this mode, simply close figure 100.
 

\item When using the spectrum explorator, the option  \mlcode{'PlotModeOptions'} allows to specify as a cell-array a list of options which will be passed to \SF{Plot} when clicking on a given eigenvalue, and will be used to plot the corresponding mode. See the example in the script to understand this.

\end{itemize}

\Tips{
\item All parameters are optional. If not provided, default values will be used (see inside the driver to check the default values).

\item Single-mode computation can be much faster than multiple-mode but requires a carefully chosen 'shift' value located as close as possible to the actual eigenvalue, otherwise the algorithm may not converge.

The recommended procedure is to first perform a "broad exploration" of the spectrum in multiple-mode computation  to localize the physically relevant eigenmodes, and then to use single-mode computation to follow these as functions of the parameters.

\item For multiple mode computation, the library ARPACK is used by default. If available on your system, you may use SLEPC by adding a line \ffcode{macro EIGENSOLVER  SLEPC //EOM} in your \fffile{SF\_Custom.idp} file (see next sectiion on customization for usage of this file).

\item The function can be used in one-output mode as \mlcode{ev = SF\_Stability(...)}
or in two-output mode as \mlcode{[ev,em] = SF\_Stability(...)}. In one-output mode the eigenmodes are not stored in the database and cannot be reaccessed later on. This can be useful if one wants to limitate the memory size of this database.

\item As for base flow calculation, you may specify your own solver using option \mlcode{'solver'} (see remark in the previous section on usage of this feature). 


}  





\subsection{ Solving a linear problem in harmonic regime : \SF{LinearForced}}

(to be completed)

Some illustrations from the 'VIV' case by Diogo

\subsection{ Advanced stability solvers : Harmonic balance, etc...}

(to be completed)

A few words on  Harmonic Balance (\SF{HB1} and \SF{HB2}), 
structural sensitivity (\SF{Sensitivity}), Weakly nonlinear computations (\SF{WNL}), etc...

\begin{figure*}[p]
\setlistfreefem
\lstinputlisting{../../SOURCES_FREEFEM/Newton_Template.edp}
\setlistmatlab
\lstinputlisting{LOG_SFBASEFLOW.txt}
\caption{
Simplified version of the program \fffile{Newton\_2D.edp} (top) and result of the execution through the driver 
\SF{BaseFlow} (bottom)}
\label{Newton_Template.edp}
\end{figure*}


%\subsection{ Using StabFem in postprocess mode: \SF{Status} and \SF{Load} } 

%(to be completed) 

%See the example  \mlfile{CYLINDER\_LINEAR\_POSTPROCESS.m}.



\section{How does it work ?  } 

\begin{figure*}[p]
\setlistfreefem
\lstinputlisting{../../SOURCES_FREEFEM/Stab_Template.edp}
\setlistmatlab
\lstinputlisting{LOG_SFSTABILITY.txt}
\caption{
Simplified version of the program \fffile{Stab\_2D.edp} (top) and result of the execution through the driver 
\SF{Stability} (bottom).}
\label{Stab_Template.edp}
\end{figure*}

%\clearpage




\subsection{Analysis of the FreeFem codes}

As explained previously, for the class of problems '2D', the baseflow and stability drivers will respectively launch the \freefem programs \fffile{Newton\_2D.edp} and  \fffile{Stab2D.edp}.
These programs, like all FreeFem solvers of the project, are located within the directory \shell{FREEFEM\_SOURCES}. 

Figures \ref{Newton_Template.edp} and \ref{Stab_Template.edp} give a simplified version of these programs, allowing to understand their structure. 
Both of these program have a similar general structure, namely :

\begin{itemize}
\item The prologue includes two files, \ffcode{SF\_Tools.idp} containing generic macros and functions common to all stabfem solvers, and \ffcode{SF\_Custom.idp} containing customizable macros (see next section). Then the working path set in the initialization (here \shell{./WORK/} ) is detected using the function \ffcode{SFDetectWorkDir}.

%\clearpage

\item Next sections (chapters 1-2) is input of parameters and reading the input files (here mesh and baseflow).  

\item Next (chapter 3-4-5) comes the core of the program. Note the systematic use of macros 
which allow to express the problems to be solved in a very synthetic way.

\item The last section (chapter 6) is post-processing and generating the output files.

\end{itemize}

To better understand, we recommend that you have a look at the full programs in the directory \shell{FREEFEM\_SOURCES}. It is also advised to try to use them directly from a bash terminal (see section ???).

\Remarks{
 
\item  In the "input of parameters" section of the present examples, the parameters are read from the standard FreeFem input (\ffcode{cin}) which allows to use the programs outside of the drivers (see section ???). When using the drivers the parameters are "piped" as described in section \ref{sec:SFLaunch}. An alternative way used in some more advanced solvers is through the \ffcode{getARGV}.
 
 \item In both programs, note that the output files are initially written direclty in the working path (here as \shell{./WORK/baseflow.txt}). At importation by the driver the file are stored in subfolders of this directory under generic names (here  \shell{./WORK/BASEFLOWS/FFDATA\_2.txt}). This manipulation allows to reaccess the data produced at any stage of the work (see section ?? on database management).

\item 
 In analysing the Baseflow example (figure \ref{Newton_Template.edp}), note that the resulting structure comprise both fields imported directly from .txt file (fields \mlcode{ux,uy,p})  and auxiliary field imported from the .ff2m file  (fields \mlcode{psi,vort}). 
 
 \item Note further that the $Re$ parameter is imported from both the .ff2m file (as metadata) and from the .txt file (as last line of the file). This redundancy is justified by the fact that the .txt file may be re-used by subsequent freefem solvers while the .ff2m file is only used by the driver.

\item The object \ffcode{INDEXING} contains all the metadata found in the corresponding .ff2m file and is used to generate an index of the database.

\item In the stability solver, the eigenvalue computation is done using the macro \ffcode{SFEVSOLVE}. This macro, which is defined in \fffile{SF\_Tools.idp}, uses either ARPACK, SLEPC, or single-mode shift-invert depending upon the value of $nev$ and upon which solver is specified in  \fffile{SF\_Custom.idp}.
 }
 
 



\subsection{The use of customizable macros}

\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../SOURCES_FREEFEM/SF_Custom_Template.idp}
\caption{
Example of file \fffile{SF\_Custom.idp} featuring a customized macro \ffcode{BoundaryconditionsBaseFlow}. }
\label{SF_Custom.edp}
\end{figure*}
Note that several sections of the codes are identified as {{\em customizable macros}. 
The customizable macros used in the present exemples comprise:

\begin{itemize}
\item Input of parameters : macros \ffcode{SFInputParametersNewton } and 

\ffcode{SFInputParametersStability}.

\item Boundary conditions : macros \ffcode{BoundaryconditionsBaseFlow} and 

\ffcode{BoundaryconditionsStability}.

\item Post-processing : macros \ffcode{SFWriteBaseFlow} and \ffcode{SFWriteMode}. 
These macro define what will be post-processed through the .ff2m files. 

\item Miscelaneous:  \ffcode{NormalizeMode}  (to normalize the eigenmodes in a prescribed way) \ffcode{CUSTOMnu} (to be used if your nondimensionalization is non standard and the nondimensional visscosity is something else that  $\nu = 1/Re$), ....

 \end{itemize}
 
 
 
This syntax is designed to allow easy customization : if you want to modify one of these parts, instead of modifying in the solvers (which are shared by several application cases and should normally not be modified) 
the recommended procedure is to copy-paste the corresponding macro in your \fffile{SF\_Custom.idp} file in your working directory and modify it there. The code will use the customized version of the macro if present, or instead the default one.

Figure \ref{SF_Custom.idp} shows the expected structure of the \fffile{SF\_Custom.idp} 
file and an example of customization. The example corresponds to the case where we want to impose a Poiseuille flow the inlet plane (boundary \#1 of the mesh) instead as a constant velocity.

\Remark{ 
In addition to customizable macros, the file fffile{SF\_Custom.idp}  may contain information about solvers to be used. It also contains a string \ffcode{STORAGEMODES} which is necessary if the .txt files produced by the solver contain vectorial data. For instance the value \ffcode{"P2,P2P2P1,P2P2P2P1"} means that the ".txt" files may contain \ffcode{[P2,P2,P1]} data (format used for instance for the base flow with two components of velocity and one pressure) or \ffcode{[P2,P2,P2,P1]} data (format used for instance for 3d eigenmodes with 3 components of velocity).
}




\section{ Database management: \SF{Status} and \SF{Load} } 

(to be completed) 

See the example  \mlfile{CYLINDER\_LINEAR\_POSTPROCESS.m}.



\section{How to add your own cases in StabFem } 

At this stage, you should have understood enough about the working logic of the StabFem project to begin incorporating your own cases of study. This section will explain how to proceed.

\subsection{Anatomy of a StabFem project}

Each StabFem study consists of a set of files contained in a directory which should be either a sobfolder of 
\shell{DEVELOPMENT\_CASES} (for still ongoing studies) or  \shell{STABLE\_CASES} (for completed studies).

Such a directory should contain :

\begin{enumerate}
\item The \mlcode{*.m} files corresponding to the Matlab/Octave scripts. Normally, a main script should be present, e.g. \mlfile{MyCase.m} ;

\item At least one mesh-generating FreeFem file, e.g.  \fffile{Mesh\_Mycase.edp}.

\item The file \fffile{SF\_Custom.idp} containing customization choices. 

\item Optionally, the directory may contain some Matlab/Octave functions specific to your cases and used by your main script. For instance, many cases contain a function  \mlcode{SmartMesh.m} which generates a well-suited mesh starting with the .edp file and performing a series of mesh adaptations.

\item For "Stable" cases, the directory should also contain an \mlfile{autorun.m} program, 
which is designed to be used as an automatic non-regression test (see section ??). 


\end{enumerate}



%Once your study is sufficiently advanced, it is advised that you switch your files from the  \shell{DEVELOPMENT\_CASES} directory to the \shell{STABLE\_CASES} directory and that you create an 
%\mlfile{autorun.m} program performing elementary tests for your case. 

%If pushed on gitlab, such \mlfile{autorun.m} program will serve as automatic non-regression tests : namely, an automatic test will be launched after each new 'push' to the gitlab repository, to check that all cases are still operational.

\subsection{  Adapting from an existing case }

If the case you wish to study belongs to a class of problems already implemented in the project, then you should not have to modify any of the .m / .edp programs in the common directories. 
All adaptations to your cases should be made in your mesh generator program and possibly in the customizable macros.

The recommended procedure is to start from one of the cases already present in the project and close to the one you want to study, copy-paste the mesh-generator and custom-macro files in a new directory, and start modifying them.

For instance, if you want to study the wake of a 2D blunt body with elliptic section, you could start from the "CYLINDER" case and adapt to your needs.


\Remark{ 

If you want to try modifications of the solver which require larger modifications than the customizable sections, 
If is also possible to tell the driver to use an alternative solver instead of the default one. For instance, 
 \mlcode{ bf= SF\_Baseflow(bf,'Re',10,'customsolver','Newton\_2D\_modified.edp')} will lanch a baseflow computation using \fffile{Newton\_2D\_modified.edp} instead of the default \fffile{Newton\_2D.edp}. In this case the altenative solver will generally be in the current directory, not in the common one. The same can be done using \SF{Stability} and a number of other generic drivers. Of course, this method requires that the input/output of the modified solvers respect the same syntax as the default one.
 
 
This way of proceeding may be used in 'developing stage' to test new ideas, but should not be used any more  once your case has reached the "stable" status.

}


\subsection{ Creating a new class of problems }
 
A number of classes of problems (incompressible, compressible, spring-mounted, etc...) are already incorporated in the project. However, it may turn out that the case you want to consider is not already present in the project.
For instance, you are intested in Non-Newtonian flow with a given rheological law, or a MHD situation requiring to consider electric/magnetic fields, etc...

In this case you will have to design your own Baseflow/Stability solvers (and possibly a few more depending on your needs) and "plug" them into the drivers.

The recommended procedure is as follows:

\begin{enumerate}
\item First, design your FreeFem solvers and validate them outside of the StabFem interface.
(If you are already a	FreeFem user you may already have those programs at hand.)

\item Second, redesign them to follow as close as possible the general structure of the templates presented in this chapter. (At minimum, you should make sure that the input/output files have the same names and are located in a working directory \ffcode{ffdatadir} detected using \ffcode{SFDetectPath})

\item Third, you will have to incorporate the solvers into the drivers. When looking into the drivers, you will see that the selection is made using a \mlcode{switch / case} branching structure according to the keyword \mlcode{problemtype} used while creating the mesh.  Create your own keyword to identify your cases and add it to the list ! 

You may also have to add your own set of parameters and specify how to pass them to the \freefem solvers. Look at the available cases and adapt to yours.

\end{enumerate}

At this stage you are ready to use all the powerful facilities offered by the StabFem interface (including mesh adaptation, database management etc...). Have fun !

\subsection{Creating an autorun} 

If you have succeeded in implementing your own class of problems, 
it is highly advised that you switch it to the "stable" side of the arborescence and add an  \mlfile{autorun.m} into your directory.

Look at existing \mlfile{autorun.m} programs to see how they work : such programs  perform a series of elementary tests by comparing the results to reference values, and give a return value '0' if everything performs as expected, and a non-zero value otherwise.

All  \mlfile{autorun.m} programs present in the gitlab base of the project will be launched automatically each time new developments are "pushed", and an advice will be sent by e-mail to developers and maintainers in case of failure. It is thus highly advised to add such cases to the project, to be sure that your cases will remain compatible with future developments !





 






 







%\clearpage


%\clearpage




\section{ Using StabFem solvers outside of Matlab/Octave} 

\begin{figure*}[p]
\setlistshell
\lstinputlisting{StabFemOutsideDrivers.txt}
\caption{Demonstration on how to use StabFem solvers directlty in a shell terminal} 
\label{fig:stabfemoutsidematlab}
\end{figure*}

If you don't have (or don't like) the Matlab/Octave environment, you can perfectly use the \freefem part of the StabFem project directly in a bash terminal !
 
For instance figure \ref{fig:stabfemoutsidematlab} shows how to perform the mesh adaptation and eigenmode computation (basically equivalent the example script at the beginning of this chapter) in bash mode.

Note that this 'old-style' method requires to carefully copy the .txt generated by \freefem into the names expected at input of next solvers. The Matlab/Octave drivers do all these manipulations automatically, and further automatically store these files in an indexed database.



\iffalse
A \stabfem project have a directory, for example \directory{StabFemm>CYLINDER} where it can be found: 
\begin{enumerate}
\item The \mlcode{*.m} files corresponding to the \ti{Matlab} scripts. Normally, a main script can be found, e.g. \fffile{SCRIPT\_CYLINDER\_DEMO.m} ;
\item A directory \directory{StabFemm>CYLINDER > WORK} (if not, it will be created when \ti{Matlab} script run) where it can be found the output data;
\item All the \mlcode{*.edp} specific to the project and to be editexecuted automatically by the \ti{Matlab} interface. E.g.: The \fffile{Mesh*.edp}, the \fffile{Macros\_StabFem.edp}, \fffile{Param\_Adaptmesh.edp}, etc.
\end{enumerate}




\subsection{Running logic of a \stabfem project}


When one executes the main script \mlcode{*.m}, both local and share scripts are executed. For a commum project, the following steps done:

\begin{enumerate}
\item Launch the share script \fffile{SF\_Start.m}:
\begin{lstlisting}
run('../SOURCES_MATLAB/SF_Start.m');
\end{lstlisting}
creating the following directories as global variables and adding the \ti{sfdir} to the \ti{matlab} paths:
\begin{lstlisting}
ff = '/PRODCOM/Ubuntu16.04/freefem/3.51/gcc-5.4-mpich_3.2/bin/\freefem'; % on IMFT network
sfdir = '~/StabFem/SOURCES_MATLAB/'; % where to find the matlab drivers
ffdir = '~/StabFem/SOURCES_FREEFEM/'; % where to find the freefem scripts
ffdatadir = './WORK/';
addpath(sfdir);
\end{lstlisting}
It also creates the \fffile{SF\_Geom.edp} need for \ti{FreeFemm++}.


\item Then, a mesh and a baseflow are generated for the project with the help of the share script \fffile{SF\_Init.edp} located in \directory{StabFemm>SOURCES\_MATLAB},e.g.:

\begin{lstlisting}
baseflow=SF_Init('Mesh_Cylinder.edp', [-40 80 40]);
\end{lstlisting}

The detailed input/output parameters are discussed in next chapter (\textcolor{red}{To do...}). It will execute \fffile{Mesh*.edp} and generate in the current path the \fffile{mesh.msh}, \fffile{mesh.ff2m}, \fffile{mesh\_init.msh},\fffile{SF\_Init.ff2m}, \fffile{BaseFlow\_init.txt} and \fffile{BaseFlow\_init.ff2m} files. 

The path \directory{StabFemm>CYLINDER > WORK>BASEFLOWS} is created. This is where all the base flow for the different Reynolds numbers will be stored.



\item The baseflows for for different parameters ($Re$, Porosity,...) are generated by the command: 
\begin{lstlisting}
baseflow=SF_BaseFlow(baseflow,'Re',10);
\end{lstlisting}
executing the commom scritpt \fffile{SF\_BaseFlow.m}. This script will read the \ti{baseflow.mesh.problemtype} parameter and execute the corresponding Newton routing \mlcode{Newton*.edp} located at \directory{StabFemm>SOURCES\_FREEFEM} in order to generate the corresponding baseflows in their path.


\item Then, a adaptation of the mesh is made to refine and converge the results for the correct baseflow, with the following command:
\begin{lstlisting}
baseflow=SF_Adapt(baseflow,'Hmax',10,'InterpError',0.005);
\end{lstlisting}
The refine parameters are written in \fffile{Param\_Adaptmesh.edp}\textcolor{red}{(why?)} in the current path. \fffile{Adapt\_Mode.edp} is executed from \ti{ffdir} to refine the mesh. \textcolor{red}{(Some files are created...explain...)}

\item A mesh adaptation taking into account the eigenmode can be done: for that, first a solution have to be commuted first giving the eigenmodes. Then the mesh adaptation can be done like it was done in last step. 
\begin{lstlisting}
[ev,em] = SF_Stability(baseflow,'shift',0.04+0.74i,'nev',1,'type','D');
[baseflow,em]=SF_Adapt(baseflow,em,'Hmax',10,'InterpError',0.01);
\end{lstlisting}

Here, the eigenvalue problem has been solved with a shitf-and-invert iteration process, detail in next chapter(\textcolor{red}{(to do)}). \fffile{SF\_Stability.m} is once again located at \ti{sfdir}.

\item After the former step, once wisely used, post-processing can be made. At that stage, each project have its particularities and it will be detailed in their dedicated chapters.
\end{enumerate}

\fi
\iffalse
\subsection{Create a \stabfem project}

In order to create a project in \stabfem one has to start to code the scripts in \ti{FreeFEM++}.

\medskip

\begin{leftbar}
Attention:  When creating the different \mlcode{*.edp} files, one has to pay attention on the compulsory inputs and output of the \ti{Matlab} interface.
\end{leftbar}

Then, the \ti{Mathlab} scripts, with the previously presented style, have to be created.

\medskip

\begin{leftbar}
Attention:  The different \mlcode{*.m} files created in the particular directory will be used only in your project, so you can used them as you like; but, once the common files are used, they must not be changed without careful examination of the impact on the other projects.
\end{leftbar}


Commonly, the following files are needed in the current file:

\paragraph{\ti{FreeFEM++} file: SCRIPT\_*.edp}

Here, the problem is defined. See chapter \textcolor{red}{(to do)} for more details. It can be a eigenvalue problem, a forced problem, etc. To define the problem, see the FreeFEM++ documentation \cite{FFdoc}\footnote{http://www.freefem.org/ff++/ftp/freefem++doc.pdf}.

\paragraph{\ti{FreeFEM++} file: Mesh\_*.edp}

File where the mesh of the problem and the convenient files are generated.

\paragraph{\ti{FreeFEM++} file: Macros\_StabFem.edp}

Macros are a powerful tool in \ti{FreeFEM++}. In this file all the Macros specific of the project are created. This macros will be used both by the \mlcode{*.edp} scripts of one's problem and by the  common scripts located at \directory{StabFemm>SOURCES\_FREEFEM}. 


\paragraph{\ti{Matlab} file: mains\_cript.m}

This script will be organise like in the previously presented style. Then, the a  different treatment is given to each problem, and one can be inspired by the project already created.
\fi

