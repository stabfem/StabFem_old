addpath('../../SOURCES_MATLAB/')
SF_Start('verbosity',4);

r0 = 0.01;
rcol = .1;
L = 1;
ffmesh = SF_Mesh('Mesh_rectangle_dirichlet.edp','Params',[r0, rcol, L],'problemtype','Kaptsov');
bf = SF_BaseFlow(ffmesh,'Eac',1);

figure; SF_Plot(bf,'phi');
pause(0.1);

%rline = [r0:0.005:L];
%philine = SF_ExtractData(bf,'phi',rline,0)
%figure; plot(rline,philine);

bfNew = SF_Adapt(bf,'Hmax',.1);

figure; subplot(2,1,1); SF_Plot(bf,'mesh')
subplot(2,1,2); SF_Plot(bfNew,'mesh')

figure; SF_Plot(bf,'phi');
figure; SF_Plot(bf,'n');
pause(0.1);

