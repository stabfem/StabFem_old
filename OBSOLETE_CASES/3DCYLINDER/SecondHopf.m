alpha = [[14.4:-0.1:12.5]];
ResS_tab = nan*ones(size(alpha)); 
Re_RangeS = [97.0:1.0:103.0];
omegaS_tab = nan*ones(size(alpha));
omegaS_tab(1) = 0.01;
omegasSguess = 0.01;
k_Range = [0.00001];
kS_tab = 0.00001*ones(size(alpha));
status_tab = nan*ones(size(alpha));
Ma = 0.0;

for i=1:length(alpha)
    % Update parameter
    Omegax = alpha(i);
    % Update mesh
    [bf,EVS,ResS,omegasS,KOut,status] = SF_Stability_LoopBisRe(bf,Re_RangeS,k_Range,omegasSguess*1i,'Mach',Ma,...
                                            'Omegax',Omegax);
    % Save current values
    ResS_tab(i) = ResS;
    omegaS_tab(i) = omegasS;
    if(i<2) % guess = start value
        ResSguess = ResS_tab(1); omegasSguess = omegaS_tab(1); kGuess = kS_tab(1);
        Re_RangeS = [ResSguess*0.95:ResSguess*.05:1.25*ResSguess];
    elseif(i<length(alpha)) % guess = extrapolation using two previous points
        ResSguess = interp1(alpha(1:i),ResS_tab(1:i),alpha(i+1),'spline','extrap');
        Re_RangeS = [ResSguess*0.95:ResSguess*0.05:1.1*ResSguess];
        % omegasSguess = interp1(alpha(1:i),omegaS_tab(1:i),alpha(i+1),'spline','extrap')
        omegasSguess = interp1(alpha(1:i),omegaS_tab(1:i),alpha(i+1),'spline','extrap');
        kGuess = interp1(alpha(1:i),kS_tab(1:i),alpha(i+1),'spline','extrap');
    end
    %k_Range = [kGuess*0.9:(kGuess*.2+0.1)/4.0:kGuess*1.1+0.1];
    k_Range = [0.00001];
end

% % 
% bf=SF_BaseFlow(bf,'Re',100.9031,'Omegax',14.16);
% [ev,em] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% 
% bf=SF_BaseFlow(bf,'Re',102.5031,'Omegax',14.16);
% [ev2,em2] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% 
% bf=SF_BaseFlow(bf,'Re',103.5031,'Omegax',14.16);
% [ev3,em3] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% 
% bf=SF_BaseFlow(bf,'Re',103.0031,'Omegax',14.16);
% [ev4,em4] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% 
% gR = real([ev(1),ev2(1)]);
% ReTh = interp1(gR,[100.7731,102.5031],0,'linear','extrap')
% 
% 
% 
% 
% bf=SF_BaseFlow(bf,'Re',104.0031,'Omegax',14.1);
% [ev5,em5] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% 
% bf=SF_BaseFlow(bf,'Re',102.5031,'Omegax',14.1);
% [ev6,em6] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% 
% bf=SF_BaseFlow(bf,'Re',103.0031,'Omegax',14.15);
% [ev2,em2] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% 
% bf=SF_BaseFlow(bf,'Re',103.2531,'Omegax',14.15);
% [ev7,em7] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% 
% bf=SF_BaseFlow(bf,'Re',105.0031,'Omegax',14.15);
% [ev3,em3] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% 
% 
% 
% % bf=SF_BaseFlow(bf,'Re',147.7731,'Omegax',14.5);
% % [ev,em] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% % bf=SF_BaseFlow(bf,'Re',142.7731,'Omegax',14.5);
% % [ev2,em2] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% % bf=SF_BaseFlow(bf,'Re',137.7731,'Omegax',14.5);
% % [ev3,em3] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% % bf=SF_BaseFlow(bf,'Re',130.7731,'Omegax',14.5);
% % [ev4,em4] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% % bf=SF_BaseFlow(bf,'Re',120.7731,'Omegax',14.5);
% % [ev5,em5] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% % bf=SF_BaseFlow(bf,'Re',110.7731,'Omegax',14.5);
% % [ev6,em6] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% % bf=SF_BaseFlow(bf,'Re',100.7731,'Omegax',14.5);
% % [ev7,em7] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% % bf=SF_BaseFlow(bf,'Re',95.7731,'Omegax',14.5);
% % [ev8,em8] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% % 
% % gR = real([ev7(1),ev8(1)]);
% % ReTh = interp1(gR,[100.77,95.77],0,'linear','extrap')
% 
% % 
% % 
% % bf=SF_BaseFlow(bf,'Re',118.001,'Omegax',12.8);
% % [ev5,em5] = SF_Stability(bf,'k',0.00001,'shift',0.01i,'nev',5,'type','D'); % Possibly in the wrong branch stable c.c.
% % 
% % gR = real([ev5(1),ev6(1),ev7(1)]);
% % ReTh = interp1(gR,[118,121,122],0,'linear','extrap')
% % alpha(29) = 12.8
% % ResS_tab(29) = 121.9
% % omegaS_tab(29)=0.0103
% % 
% % plot(ResS_tab,alpha)
% % plot(ReS,alphaS)